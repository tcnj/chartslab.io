## GitLab Helm Charts

This repository contains GitLab's official Helm charts. [Helm](https://helm.sh/) is a package manager for
Kubernetes, making it easier to deploy, upgrade, and maintain software like GitLab.
The charts are automatically published to our Helm repo, located at [charts.gitlab.io](https://charts.gitlab.io).

This repository currently has the following charts:

| Chart name   | Status       | Remark
|--------------|--------------|----------|
| gitlab-omnibus | Beta | Suitable for small deployments, will be replaced by the [cloud native GitLab chart](#cloud-native-gitlab-chart). |
| gitlab-runner | GA | Deploys the GitLab CI/CD Runner. |
| gitlab       | Deprecated | Should not be used. |
| kubernetes-gitlab-demo | Deprecated | Should not be used. |
| auto-deploy-app | Beta | Used by [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/topics/autodevops/index.md). |

More information is available in our [chart documentation](https://docs.gitlab.com/ce/install/kubernetes/).

### Cloud Native GitLab Chart

We are building a cloud native GitLab chart in a separate repo at [helm.gitlab.io](https://gitlab.com/charts/helm.gitlab.io). The goal of that work
is to replace the `gitlab` and `gitlab-omnibus` charts in this repository as well as [the community supported charts](https://github.com/kubernetes/charts/tree/master/stable/gitlab-ce). We plan
to launch the cloud native chart in this repository with the `gitlab` name once it is ready.

A migration will be required to move from the `gitlab-omnibus` chart to the cloud native GitLab chart. We plan for this to be accomplished by an upcoming [backup option](https://gitlab.com/charts/charts.gitlab.io/issues/27) for the current instance and restoring on the new one.

### Usage

To use the charts, the Helm tool must be installed and initialized. The best
place to start is by reviewing the [Helm Quick Start Guide](https://github.com/kubernetes/helm/blob/master/docs/quickstart.md).

Installation instructions, including configuration options, can be found in our [documentation](http://docs.gitlab.com/ce/install/kubernetes/).

### GitLab Helm Charts Issue Tracker

Issues related to the Charts can be logged at: <https://gitlab.com/charts/charts.gitlab.io/issues>

### Contributing to the Charts

We welcome contributions and improvements. The source repo for our Helm Charts can be found here: <https://gitlab.com/charts/charts.gitlab.io>

Please see the [contribution guidelines](CONTRIBUTING.md)
